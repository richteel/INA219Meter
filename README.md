View the [Wiki](https://gitlab.com/richteel/INA219Meter/wikis/home) for information regarding this project.

<img src="Docs/Photos/DSC03277.JPG" alt="The INA219 Meter reading voltages and current on the TOS Tricorder" style="width: 50%; height: 50%" />

For this project plus others, check put my website at <a href="http://www.teelsys.com">www.teelsys.com</a>