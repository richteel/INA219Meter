/*
  ** Sparkfun Nokia 5110 connections **
  Graphic LCD Pin ----------- Teensy 3.1
     1-VCC        ----------- 3V
     2-GND        ----------- GND
     3-SCE        ----------- D4
     4-RST        ----------- D3
     5-D/C        ----------- D5
     6-DN(MOSI)   ----------- D11/DOUT
     7-SCLK       ----------- D13/SCK
     8-LED        - 220 Ohm - D21

  ** Other connections **
  Item      ----------- Teensy 3.1
  Switch    ----------- D12
  Green LED ----------- D10
  Red LED   ----------- D9
*/

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include <Adafruit_INA219.h>

#define VERSION "0.0.1"
#define DELIMITER_COL "\t"
#define DELIMITER_ROW "\r\n"

/* PIN Assignments */
/* LCD */
#define LCD_SCE 4
#define LCD_RST 3
#define LCD_DC 5
#define LCD_DN 11
#define LCD_SCLK 13
#define LCD_LED 21
/* Other */
#define SWITCH 12
#define LED_RED 9
#define LED_GREEN 10

#define DISPLAYUPDATEDELAY 1000
#define SWITCHSHORTPRESS 50
#define SWITCHLONGPRESS 500
#define BACKLIGHTBRIGHTNESS 128
#define DISPLAYCONTRAST 55
#define NOMINALVOLTAGE 3.7

enum displays {
  METER,
  RUNTIME,
  MINMAX,
  UTILITY,
  ABOUT
};

enum utilityCommands {
  RESETPOWER,
  RESETMINMAX,
  RESETBOTH,
  EXITUTILITY
};

Adafruit_INA219 ina219(0x40);
// Hardware SPI (faster, but must use certain hardware pins):
Adafruit_PCD8544 display = Adafruit_PCD8544(LCD_DC, LCD_SCE, LCD_RST);

/* Fields */
unsigned int counts = 0;
float vShunt_sum = 0;
float vBus_sum = 0;
float current_sum = 0;

float vShunt = 0;
float vBus = 0;
float current = 0;
float vMin = 9999;
float vMax = 0;
float iMin = 9999;
float iMax = 0;
float vLoad = 0;
float power = 0;
float wattHours = 0;
float ampHours = 0;
unsigned long powerStartMillis = 0;
unsigned long greenLedMillis = 0;
unsigned long startTimeMillis = 0;

// button
int lastButtonState = LOW;   // the previous reading from the input pin
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
bool buttonPressShort = false;
bool buttonPressLong = false;

// Display
enum displays currentDisplay = METER;
enum utilityCommands menuItem = EXITUTILITY;
unsigned long previousDisplayMillis = 0;
bool backlightState = true;
bool cursorInverted = false;


/*** Function Prototypes ***/
void calculateMeasurements(unsigned long intervalTime);
void displayAbout();
void displayMeter();
void displayMinMax();
void displayRuntime(unsigned long currentMillis);
void displayUtility(unsigned long currentMillis);
void getButtonState(unsigned long currentMillis);
void on_longButtonPress();
void on_shortButtonPress();
void printSerialData();
void printSerialHeader();
void printText(int row, const String lineOfText, bool inverted=false);
void takeMeasurements();
char * TimeToString(unsigned long t);
void toggleBacklight();
void updateDisplay(unsigned long currentMillis);

void setup() {
  Serial.begin(115200);

  // Initialize meter
  ina219.begin();

  // Initialize display
  display.begin();

  // Turn on the backlight
  pinMode(LCD_LED, OUTPUT);
  analogWrite(LCD_LED, BACKLIGHTBRIGHTNESS);
  display.setContrast(DISPLAYCONTRAST);
  display.display(); // show splashscreen

  // Initialize LEDs
  pinMode(LED_RED, OUTPUT);
  digitalWrite(LED_RED, HIGH);
  pinMode(LED_GREEN, OUTPUT);
  digitalWrite(LED_GREEN, LOW);

  // Initialize Switch
  pinMode(SWITCH, INPUT);

  printSerialHeader();
}

void loop() {
  unsigned long currentMillis = millis();

  takeMeasurements();

  if (digitalRead(LED_GREEN) && (currentMillis - greenLedMillis > 1000)) {
    digitalWrite(LED_GREEN, LOW);
  }

  getButtonState(currentMillis);
  if (buttonPressLong) {
    buttonPressLong = false;
    on_longButtonPress();
  }
  else if (buttonPressShort) {
    buttonPressShort = false;
    on_shortButtonPress();
  }

  if (currentMillis - previousDisplayMillis >= DISPLAYUPDATEDELAY) {
    unsigned long intervalTime = currentMillis - powerStartMillis;

    previousDisplayMillis = currentMillis;
    powerStartMillis = currentMillis;
    calculateMeasurements(intervalTime);

    printSerialData();

    updateDisplay(currentMillis);
  }
}

/*** SUPPORTING FUNCTIONS ***/
void calculateMeasurements(unsigned long intervalTime) {
  vShunt = vShunt_sum / counts;
  vBus = vBus_sum / counts;
  current = current_sum / counts;
  vLoad = vBus + (vShunt / 1000);
  power = vBus * (current / 1000);
  wattHours = wattHours + (abs(vBus * (current / 1000)) * ((float)intervalTime / 3600000));
  ampHours  = (wattHours / NOMINALVOLTAGE);
}

void displayAbout() {
  char strVer [20];
  sprintf(strVer, "Ver: %s", VERSION);

  printText(0, F("     ABOUT    "), true);
  printText(1, F("   DC Meter   "), false);
  printText(2, F(""), false);
  printText(3, F(" TeelSys LLC  "), false);
  printText(4, F("     2016     "), false);
  printText(5, strVer, false);
}

void displayMeter() {
  char strVBus [20];
  char strVShunt [20];
  char strVLoad [20];
  char strCurrent [20];
  char strPower [20];
  sprintf(strVBus,    "B:%9.3f V ", vBus);
  sprintf(strVShunt,  "S:%9.3f mV", vShunt);
  sprintf(strVLoad,   "L:%9.3f V ", vLoad);
  sprintf(strCurrent, "C:%9.3f mA", current);
  sprintf(strPower,   "P:%9.3f W ", power);

  printText(0, F("     METER    "), true);
  printText(1, strVBus, false);
  printText(2, strVShunt, false);
  printText(3, strVLoad, false);
  printText(4, strCurrent, false);
  printText(5, strPower, false);
}

void displayMinMax() {
  char strMinV [20];
  char strMaxV [20];
  char strMinI [20];
  char strMaxI [20];
  sprintf(strMinV, "MIN:%7.2f V", vMin);
  sprintf(strMaxV, "MAX:%7.2f V", vMax);
  sprintf(strMinI, "MIN:%7.2f mA", iMin);
  sprintf(strMaxI, "MAX:%7.2f mA", iMax);

  printText(0, F("    Min Max   "), true);
  printText(1, F(""), false);
  printText(2, strMinV, false);
  printText(3, strMaxV, false);
  printText(4, strMinI, false);
  printText(5, strMaxI, false);
}

void displayRuntime(unsigned long currentMillis) {
  char strRunTime [20];
  char strMah [20];
  char strMwh [20];
  unsigned long timeSeconds = (currentMillis - startTimeMillis) / 1000;
  sprintf(strRunTime, "Time:%9s", TimeToString(timeSeconds));
  sprintf(strMah,     "%10.3f mAh", ampHours * 1000);
  sprintf(strMwh,      "%10.3f mWh", wattHours * 1000);

  printText(0, F("   Run Time   "), true);
  printText(1, strRunTime, false);
  printText(2, F("   Amp Hours  "), false);
  printText(3, strMah, false);
  printText(4, F("  Watt Hours  "), false);
  printText(5, strMwh, false);
}

void displayUtility(unsigned long currentMillis) {
  cursorInverted = !cursorInverted;

  char strRunTime [20];
  unsigned long timeSeconds = (currentMillis - startTimeMillis) / 1000;
  sprintf(strRunTime, "Time:%9s", TimeToString(timeSeconds));

  printText(0, F("    Utility   "), true);
  printText(1, strRunTime, false);
  printText(2, F("  Reset Power "), menuItem == RESETPOWER ? cursorInverted : false);
  printText(3, F(" Reset Min/Max"), menuItem == RESETMINMAX ? cursorInverted : false);
  printText(4, F("  Reset Both  "), menuItem == RESETBOTH ? cursorInverted : false);
  printText(5, F(" Exit Utility "), menuItem == EXITUTILITY ? cursorInverted : false);
}

void getButtonState(unsigned long currentMillis) {
  int reading = digitalRead(SWITCH);

  if (reading != lastButtonState) {
    if (reading == LOW) { // Switch released
      if (currentMillis - lastDebounceTime >= SWITCHLONGPRESS) { // Long press
        buttonPressLong = true;
      }
      else if (currentMillis - lastDebounceTime >= SWITCHSHORTPRESS) { // Short press
        buttonPressShort = true;
      }
    }

    // reset the debouncing timer
    lastDebounceTime = currentMillis;
    lastButtonState = reading;
  }
}

void on_longButtonPress() {
  if (currentDisplay == UTILITY) {
    switch (menuItem) {
      case RESETPOWER:
        printSerialHeader();
        startTimeMillis = millis();
        wattHours = 0;
        break;
      case RESETMINMAX:
        printSerialHeader();
        vMin = 9999;
        vMax = 0;
        iMin = 9999;
        iMax = 0;
        break;
      case RESETBOTH:
        printSerialHeader();
        startTimeMillis = millis();
        wattHours = 0;
        vMin = 9999;
        vMax = 0;
        iMin = 9999;
        iMax = 0;
        break;
      default:
        currentDisplay = ABOUT;
        previousDisplayMillis = 0; // Cause the display to redraw immediately
        break;
    }
    digitalWrite(LED_GREEN, HIGH);
    greenLedMillis = millis();
  } else {
    toggleBacklight();
  }
}

void on_shortButtonPress() {
  switch (currentDisplay) {
    case METER:
      currentDisplay = RUNTIME;
      break;
    case RUNTIME:
      currentDisplay = MINMAX;
      break;
    case MINMAX:
      currentDisplay = UTILITY;
      break;
    case UTILITY:
      switch (menuItem) {
        case RESETPOWER:
          menuItem = RESETMINMAX;
          break;
        case RESETMINMAX:
          menuItem = RESETBOTH;
          break;
        case RESETBOTH:
          menuItem = EXITUTILITY;
          break;
        case EXITUTILITY:
        default:
          menuItem = RESETPOWER;
          break;
      }
      cursorInverted = false;
      break;
    case ABOUT:
    default:
      currentDisplay = METER;
      break;
  }

  previousDisplayMillis = 0; // Cause the display to redraw immediately
}

void printSerialData() {
  unsigned long timeSeconds = (millis() - startTimeMillis) / 1000;
  char strVBus [20];
  char strVShunt [20];
  char strVLoad [20];
  char strCurrent [20];
  char strPower [20];
  char strAmpHours [20];
  char strWattHours [20];
  char strMinVolts [20];
  char strMaxVolts [20];
  char strMinCurrent [20];
  char strMaxCurrent [20];
  sprintf(strVBus,    "%0.3f", vBus);
  sprintf(strVShunt,  "%0.3f", vShunt);
  sprintf(strVLoad,   "%0.3f", vLoad);
  sprintf(strCurrent, "%0.3f", current);
  sprintf(strPower,   "%0.3f", power);
  sprintf(strAmpHours,   "%0.3f", ampHours * 1000);
  sprintf(strWattHours,   "%0.3f", wattHours * 1000);
  sprintf(strMinVolts,   "%0.3f", vMin);
  sprintf(strMaxVolts,   "%0.3f", vMax);
  sprintf(strMinCurrent,   "%0.3f", iMin);
  sprintf(strMaxCurrent,   "%0.3f", iMax);

  Serial.print(timeSeconds);
  Serial.print(DELIMITER_COL);
  Serial.print(strVBus);
  Serial.print(DELIMITER_COL);
  Serial.print(strVShunt);
  Serial.print(DELIMITER_COL);
  Serial.print(strVLoad);
  Serial.print(DELIMITER_COL);
  Serial.print(strCurrent);
  Serial.print(DELIMITER_COL);
  Serial.print(strPower);
  Serial.print(DELIMITER_COL);
  Serial.print(strAmpHours);
  Serial.print(DELIMITER_COL);
  Serial.print(strWattHours);
  Serial.print(DELIMITER_COL);
  Serial.print(strMinVolts);
  Serial.print(DELIMITER_COL);
  Serial.print(strMaxVolts);
  Serial.print(DELIMITER_COL);
  Serial.print(strMinCurrent);
  Serial.print(DELIMITER_COL);
  Serial.print(strMaxCurrent);
  Serial.print(DELIMITER_ROW);
}

void printSerialHeader() {
  Serial.print(DELIMITER_ROW);
  Serial.print(DELIMITER_ROW);
  Serial.print(DELIMITER_ROW);
  Serial.println(F("Time\tV_Bus\tV_Shunt\tV_Load\tCurrent\tPower\tAmpHours\tWattHours\tMinVolts\tMaxVolts\tMinCurrent\tMaxCurrent"));
}

void printText(int row, const String lineOfText, bool inverted) {
  int cursorPos = row * 8;
  display.setCursor(0, cursorPos);
  display.setTextColor(BLACK, WHITE);
  if (inverted) {
    display.setTextColor(WHITE, BLACK); // 'inverted' text
  }
  display.println(lineOfText);
}

void takeMeasurements() {
  // Take measurements
  float vShunt = ina219.getShuntVoltage_mV();
  float vBus = ina219.getBusVoltage_V();
  float current = ina219.getCurrent_mA();

  counts++;
  vShunt_sum = vShunt_sum + vShunt;
  vBus_sum = vBus_sum + vBus;
  current_sum = current_sum + current;

  // Get Min and Max values
  if (vBus < vMin) vMin = vBus;
  if (vBus > vMax) vMax = vBus;
  if (current < iMin) iMin = current;
  if (current > iMax) iMax = current;

  // If capturing the first data or the timer is reset, initialize the timer
  if (startTimeMillis == 0) {
    startTimeMillis = millis();
    wattHours = 0;
  }
}

// t is time in seconds = millis()/1000;
char * TimeToString(unsigned long t) {
  static char str[12];
  long h = t / 3600;
  t = t % 3600;
  int m = t / 60;
  int s = t % 60;
  sprintf(str, "%ld:%02d:%02d", h, m, s);
  return str;
}

void toggleBacklight() {
  backlightState = !backlightState;

  if (backlightState) {
    analogWrite(LCD_LED, BACKLIGHTBRIGHTNESS);
  }
  else {
    analogWrite(LCD_LED, 0);
  }
}

void updateDisplay(unsigned long currentMillis) {
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextWrap(false);

  switch (currentDisplay) {
    case METER:
      displayMeter();
      break;
    case RUNTIME:
      displayRuntime(currentMillis);
      break;
    case MINMAX:
      displayMinMax();
      break;
    case UTILITY:
      displayUtility(currentMillis);
      break;
    case ABOUT:
    default:
      displayAbout();
      break;
  }

  display.display();
}

